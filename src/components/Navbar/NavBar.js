import React from 'react'
import logo from "../../assests/long-logo.png";
import "./NavBar.css"

function NavBar() {
    return (
        <>
            <div className='navbar'>
                <img src={logo} alt="" className='navbar-img' />
                <div className='search-bar'>
                    <div className='search-bar-text'>Any Where</div>
                    <div className='search-bar-text'>Any week</div>
                    <div className='search-bar-text3'>Any Guests</div>
                    <div className='search-bar-icon-div'><i class="fa-solid fa-magnifying-glass"></i></div>
                </div>
                <div className='profile-container'>
                    <div className='airbnb-your-home'>Airbnb your home</div>
                    <div className='airbnb-your-home'> <i class="fa-sharp fa-solid fa-globe"></i></div>
                    <div class="btn-group dropstart">
                        <button class="btn btn-secondary text-secondary bg-light rounded-pill" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa-solid fa-bars"></i>
                            <i class="fa-solid fa-circle-user"></i>
                        </button>
                        <ul class="dropdown-menu ">
                            <li><a class="dropdown-item" href="#"><b>Sign Up</b></a></li>
                            <li><a class="dropdown-item" href="#">Login</a></li>
                            <div style={{ height: "1px", backgroundColor: "var(--grey)", width: "100%" }}></div>
                            <li><a class="dropdown-item" href="#">Airbnb your home</a></li>
                            <li><a class="dropdown-item" href="#">Help</a></li>
                        </ul>
                    </div>
                </div >
            </div >

        </>
    )
}

export default NavBar
