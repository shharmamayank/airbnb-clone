import React from 'react'
import { links } from '../../assests/images-links.js'
import "./FilterByCategories.css"
function FilterByCategories() {

    // const alldata =
    // console.log(alldata)
    {/* {alldata} */ }
    return (
        <>
            <div className='filter-div'>
                {links.map((data, i) => (
                    <div className="link-image-container">
                        <img key={i} src={data.imgSrc} className='links-img' />
                        <p className='links-label'>{data.label}</p>
                    </div>
                ))}
            </div>



        </>
    )

}

export default FilterByCategories
