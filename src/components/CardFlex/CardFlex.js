import React from 'react'
import "./CardFlex.css"
import hotels from '../../assests/hotels/hotel-1.jpeg'

function CardFlex() {
    return (
        <>
        <div >
            <img src={hotels} alt="" className='card-img' />
            <div className='card-info-flex'>
                <h3 className='card-title'>Place</h3>
                <div className='card-rating'>
                    <i class="fa-sharp fa-solid fa-star"></i>
                    <p>4.88</p>
                </div>
            </div>
        </div>
        </>
    )
}

export default CardFlex
