import CardFlex from "./components/CardFlex/CardFlex";
import FilterByCategories from "./components/Filter/FilterByCategories";
import NavBar from "./components/Navbar/NavBar";
function App() {
  return (
    <>
      <NavBar />
      <FilterByCategories />
      <CardFlex />
      <CardFlex />
      <CardFlex />
      <CardFlex />
      <CardFlex />
    </>
  );
}

export default App;
